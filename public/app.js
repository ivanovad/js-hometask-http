"use strict";
function addUserFetch(userInfo) {
  // напишите POST-запрос используя метод fetch
  let options = {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify(userInfo),
  };
  return fetch("/users", options)
    .then((response) => {
      if (response.ok) {
        return response.json();
      } else {
        throw new Error(
          `Запрос завершился не успешно: ${response.status}: ${response.statusText}`
        );
      }
    })
    .then((user) => user.id);
}

function getUserFetch(id) {
  // напишите GET-запрос используя метод fetch
  return fetch(`/users/${id}`)
    .then((response) => {
      if (response.ok) {
        return response.json();
      } else {
        throw new Error(
          `Запрос завершился не успешно: ${response.status}: ${response.statusText}`
        );
      }
    })
    .then((userInfo) => userInfo);
}

function addUserXHR(userInfo) {
  // напишите POST-запрос используя XMLHttpRequest
  return new Promise((resolve, reject) => {
    const xhr = new XMLHttpRequest();
    xhr.open("POST", `/users`);
    xhr.setRequestHeader("Content-Type", "application/json");
    xhr.send(JSON.stringify(userInfo));

    xhr.onload = () => {
      if (xhr.status >= 400) {
        reject(
          `Запрос завершился не успешно: ${xhr.status}: ${xhr.statusText}`
        );
      }
      resolve(JSON.parse(xhr.responseText));
    };
  }).then((user) => user.id);
}

function getUserXHR(id) {
  // напишите GET-запрос используя XMLHttpRequest
  return new Promise((resolve, reject) => {
    const xhr = new XMLHttpRequest();
    xhr.open("GET", `/users/${id}`);
    xhr.setRequestHeader("Content-Type", "application/json");
    xhr.send();

    xhr.onload = () => {
      if (xhr.status >= 400) {
        reject(
          `Запрос завершился не успешно: ${xhr.status}: ${xhr.statusText}`
        );
      }
      resolve(JSON.parse(xhr.responseText));
    };
  }).then((userInfo) => userInfo);
}

function checkWork() {
  addUserXHR({ name: "Alice", lastname: "XMLHttpRequest" })
    .then((userId) => {
      console.log(`Был добавлен пользователь с userId ${userId}`);
      return getUserXHR(userId);
    })
    .then((userInfo) => {
      console.log(`Был получен пользователь:`, userInfo);
    })
    .catch((error) => {
      console.error(error);
    });
}

checkWork();
